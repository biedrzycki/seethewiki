/**
 * @author Kamil Biedrzycki <biedrzycki.kamil@gmail.com>
 *
 */

// when the DOM's ready
$(document).ready(function () {

    "use strict";

    $.fn.resetClass = function (className) {
        this.removeClass().addClass(className);
        return this;
    };

    var routeProvider = function () {
        var newPage;
        var oldPage;

        var setupRoutes = function () {
            //setup crossroads
            crossroads.addRoute('search/{query}', function (query) {
                var amount = 25;

                if (query !== request.getSearchQuery() || router.getHashInfo()[1] === "home") {
                    $.when(request.searchWikiImages(query, amount)).then(function () {
                        galleryService.createGallery(request.getData());
                    }, function () {

                    });

                    $window.scrollTop(1);
                }

                $homeSearchContainer.hide();
                $headerSearchContainer.show();
                galleryService.showGallery();
                galleryService.hideDetails();
                galleryService.emptyDetails();
                $body.resetClass('page-search');

                $headerSearchField.val(query);
            });

            crossroads.addRoute('image/{id}/{name}', function (id, name) {
                $homeSearchContainer.hide();
                $body.resetClass('page-image');
                galleryService.hideGallery();
                galleryService.showDetails();

                $.when(request.getDetailedWikiImage(id)).then(function () {
                    galleryService.createDetails(request.getData());
                }, function () {

                });
            });

            crossroads.addRoute('home', function () {
                galleryService.hideGallery();
                galleryService.emptyGallery();
                galleryService.hideDetails();
                galleryService.emptyDetails();

                $homeSearchContainer.show();
                $headerSearchContainer.hide();
                $body.resetClass('page-home');
            });
        };

        var setupHasher = function () {
            function parseHash(newHash, oldHash) {
                newPage = newHash;
                oldPage = oldHash;

                crossroads.parse(newHash);
            }

            hasher.initialized.add(parseHash);
            hasher.changed.add(parseHash);
            hasher.init();

            // set start location
            if (location.hash.length === 0 || location.hash === '#/')
                hasher.setHash('home');
        };

        this.getHashInfo = function () {
            return [newPage, oldPage];
        };

        this.init = function () {
            setupRoutes();
            setupHasher();
        };
    };

    var requestService = function () {
        var receivedData,
            SEARCHING,
            queryOffset,
            searchQuery;

        var makeRequest = function (url) {
            var dfd = new jQuery.Deferred();

            $.get(url,function (response) {
                receivedData = response;

                if (typeof response['query-continue'] !== "undefined" && typeof response['query-continue'].search !== "undefined") {
                    queryOffset = response['query-continue'].search.gsroffset;
                } else {
                    queryOffset = null;
                }

                dfd.resolve("success");

            }, 'jsonp').fail(function () {
                    receivedData = 'ERROR';
                    queryOffset = null;
                    dfd.reject("error");
                });

            return dfd.promise();
        };

        var beginSearch = function (url, dfd) {
            SEARCHING = 1;
            $loading.fadeIn(200);

            $.when(makeRequest(url)).then(function () {
                // on success
                SEARCHING = 0;

                setTimeout(function () {
                    $loading.hide();
                }, 250);

                dfd.resolve("success");
            }, function () {
                // on fail..
                dfd.reject("error");
            });
        };

        this.getRequestState = function () {
            return SEARCHING;
        };

        this.getData = function () {
            return receivedData;
        };

        this.getOffset = function () {
            return queryOffset;
        };

        this.getSearchQuery = function () {
            return searchQuery;
        };

        this.searchWikiImages = function (q, amount, offset) {
            var dfd = new jQuery.Deferred(),
                url = 'https://commons.wikimedia.org/w/api.php?action=query&generator=search&gsrnamespace=6';

            searchQuery = q;

            url += '&gsrlimit=' + amount;
            url += '&gsrsearch=' + q;

            if (typeof offset !== "undefined") {
                url += '&gsroffset=' + offset;
            }

            url += '&prop=imageinfo&iiprop=url|user|mime&iiurlheight=400&format=json';

            beginSearch(url, dfd);
            return dfd.promise();
        };

        this.getDetailedWikiImage = function (id) {
            var dfd = new jQuery.Deferred(),
                url = 'https://commons.wikimedia.org/w/api.php?action=query';

            url += '&pageids=' + id;
            url += '&prop=imageinfo&iiprop=url|user|mime|size&iilimit=1&format=json';

            beginSearch(url, dfd);
            return dfd.promise();
        }
    };

    var galleryService = {
        createGallery: function (images) {
            this.emptyGallery();
            this.populateGallery(images);
        },

        populateGallery: function (images) {
            if (typeof images.query.pages !== "undefined") {

                $.each(images.query.pages, function (k, v) {

                    // we want only images - mime type with image/...
                    if (v.imageinfo[0].mime.indexOf('image') === -1) {
                        return 1;
                    }

                    var imginfo = v.imageinfo[0],
                        image_data = {
                            image_name: v.title.replace('File:', ''),
                            image_url: imginfo.url,
                            image_thumburl: imginfo.thumburl,
                            image_user: imginfo.user,
                            image_id: k
                        },
                        image;

                    image = ich.gallery_image(image_data);

                    $results.append(image);
                });

                this.lazyLoadImages();
            } else {
                // nothing found
                var info;
                info = ich.nothing_found();
                $results.append(info);
            }
        },

        createDetails: function (image) {
            this.emptyDetails();
            this.populateDetails(image);
        },

        populateDetails: function (images) {
            if (typeof images.query.pages !== "undefined") {

                $.each(images.query.pages, function (k, v) {
                    var imginfo = v.imageinfo[0],
                        image_data = {
                            image_name: v.title.replace('File:', ''),
                            image_url: imginfo.url,
                            image_user: imginfo.user,
                            image_wiki_url: imginfo.descriptionurl,
                            image_mime: imginfo.mime,
                            image_width: imginfo.width,
                            image_height: imginfo.height
                        },
                        image;

                    image = ich.image_full(image_data);

                    $imageDetails.append(image);
                });

                onWindowResize();
            }
        },

        emptyGallery: function () {
            $results.html('');
        },

        emptyDetails: function () {
            $imageDetails.html('');
        },

        hideGallery: function () {
            $results.hide();
        },

        showGallery: function () {
            $results.show();
        },

        hideDetails: function () {
            $imageDetails.hide();
        },

        showDetails: function () {
            $imageDetails.show();
        },

        lazyLoadImages: function () {
            $('.lazy').lazyload({
                load: function () {
                    $(this).removeClass('loading')
                }
            }).removeClass('lazy');
        }
    };

    var router;
    var request;

    //DOM variables
    var $homeSearchContainer = $('#home-search'),
        $homeSearchField = $('#search-query'),
        $headerSearchContainer = $('#search-header'),
        $headerSearchField = $('#search-query-header'),
        $results = $('#results'),
        $imageContentBox = $('#image-content').find('.image'),
        $imageDetails = $('#image-details'),
        $loading = $('#loading'),
        $window = $(window),
        $body = $('body');

    // get request services
    request = new requestService();

    // start routing
    router = new routeProvider();
    router.init();

    function prepareSearch() {
        $headerSearchContainer.fadeIn(400);
        $homeSearchContainer.fadeOut(400);
    }

    function submitHomeSearch() {
        var searchQuery = $homeSearchField.val().trim();
        if (searchQuery.length === 0) {
            $homeSearchField.addClass('error').focus();
        } else {
            prepareSearch();
            hasher.setHash('search/' + searchQuery);
        }
    }

    function submitSearch() {
        var searchQuery = $headerSearchField.val().trim();
        if (searchQuery.length === 0) {
            $headerSearchField.focus();
        } else {
            hasher.setHash('search/' + searchQuery);
        }
    }

    function loadImages() {
        // load next images when reaching end of page (500px before)
        if ($window.scrollTop() + $window.height() >= ($(document).height() - 500)) {
            if (request.getRequestState() === 0 && request.getOffset() !== null) {
                var amount = 25;

                $.when(request.searchWikiImages(request.getSearchQuery(), amount, request.getOffset())).then(function () {
                    galleryService.populateGallery(request.getData());
                }, function () {

                });
            }
        }
    }

    function onWindowResize() {
        $imageContentBox = $('#image-content').find('.image');
        var boxPadding = parseInt($imageContentBox.css('padding-top'));
        $imageContentBox.height($window.height() - (boxPadding * 2));
    }

    (function randomHomeImage() {
        var randomNumber = Math.floor(Math.random() * 5) + 1;
        $homeSearchContainer.css('background-image', 'url(images/backgrounds/bg-' + randomNumber + '.jpg)');
    })();

    (function attachEvents() {
        $('#search-media').on('submit', function (e) {
            e.preventDefault();
            submitHomeSearch();
            return false;
        });

        $('#search-media-header').on('submit', function (e) {
            e.preventDefault();
            submitSearch();
            return false;
        });

        $homeSearchField.on('keypress', function () {
            $homeSearchField.removeClass('error');
        });

        $('#search-header-begin').on('click', function () {
            if (request.getRequestState() === 0) {
                $('#search-media-header').submit();
            }
        });

        $imageDetails.on('click', '.close', function () {
            if (typeof router.getHashInfo()[1] !== "undefined") {
                //hasher.setHash(router.getHashInfo()[1]);
                history.back(-1);
            } else {
                hasher.setHash('home');
            }

            galleryService.emptyDetails();
        });

        $window.scroll(loadImages);

        $window.resize(onWindowResize).resize();
    })();

});